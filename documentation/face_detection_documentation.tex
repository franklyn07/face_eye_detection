\documentclass[11pt,a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[left=0.5in,top=0.5in,right=0.5in,bottom=0.5in,nohead,nofoot]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{pdfpages}
\usepackage{hyperref}

\title{Machine Learning Course Project}
\date{Data Accessed: \today}
\author{Franklyn Sciberras}

\begin{document}

\maketitle

\section{Introduction}

	\par
	Locating facial features and face detection in images or video can be considered as one of the most important stages in a wide array of face detection technologies. Such technologies are used in various parts of different industries where pattern matching or any variant of this approach is used, with the ultimate goal being that of producing a binary classifier that is able to minimize the misclassification risk and thus be able to detect the presence of a face in a source. Considering the variety in usage of such technologies, the variance in how critical the result of a classification is, as well as the time frame in which a result is expected, create a a varied environment which requires different kind of algorithms, whith different strengths and limitations, with regards to speed and accuracy.

	\par
	Various factors come in play when it comes to face detection technologies, such as changes in background, lighting, facial expression and the face structure and composition itself. When considering these factors along with the alogrithm requirements, based on the use of the specific algorithm, it is evident that face detection is quite a complex and challenging task. One relatively simple algorithm created to tackle such a problem, was the Viola Jones algorithm. This algorithm put together various concepts, and created one whole solution that with it's own strengths and weaknesses, proved to be one of the first object detection algorithms to gain huge popularity in this domain, and is still considered quite popular, till date. Such a success was mainly accredited to it's reliability and availability through projects such as OpenCv, that increased the reach of the algorithm, making it easy to use and thus a go\-to solution in various scenarios.

\section{Viola\-Jones Technical Discussion}
	
	\par
	The Viola\-Jones algorithm is mainly comprised of four different concepts, which when put together produce an algorithm which is able to perform face detection in real time, where the source can be either in the form of video as well as image. The aforementioned concepts are the following:

	\begin{itemize}
		\item Haar Features
		\item Integral Images
		\item Classifier Training using AdaBoost Algorithm
		\item Haar Cascades
	\end{itemize}

	\subsection{Haar Features}

		\par
		If we had to consider the generic object that we are looking for in the source, we can come up with the conclusion that all the objects, if of the same nature, will have some sort of similar properties between them. If we take the face as an example, most of the faces will be composed of a pair of visible eyes, one mouth, one nose etc. These similar features will have similar properties such as having two dark objects seperated by a lighter area in between, when considering the eyes and the nose bridge, or having a dark area surrounded by a lighter area if we had to consider the eyes section. Haar Features were created to exploit these redundancies and represent them in a generic, digital way. This was achieved by creating a set of basic blocks, comprising of different combinations of black and white regions, whith the purpose of accentuating and representing specific features. These blocks, due to their basic and digital nature, could then very easily be modified and altered in such a way that they are able to represent different sizes and compositions, of different features present on different sources. 

		\begin{figure}[ht]
		  \begin{center}
		    \includegraphics[width=0.6\linewidth]{haar_features.jpg}
		    \caption{Examples of Haar Features}
		  \end{center}
		\end{figure}

		\par
		When these Haar Features are applied to the source, and resized and modified accordingly, the features found on a possible object, are then represented by these Haar Features. These modifications and fittings are performed through a sliding window approach, where a window of a specific size (indicated in the program) is used to split the source into multiple isolated areas of scrutiny, to which the various possible Haar Features are tested for fitting. When the whole image is traversed, the window is increased in size by a specific scaling factor, and the process is repeated again. This is done untill the sliding window corresponds to a specific size, indicated in the program. If enough features are found in a specific area, that area would be classified as a possible object detected, thus the combination of these Haar Features would be acting as a strong classifier, whereas a single Haar Feature is considered as a weak classifier, since it cannot successfully classify a source on its own.. The result would be of the following form:

		\begin{figure}[ht]
		  \begin{center}
		    \includegraphics[width=0.6\linewidth]{face_representation.jpg}
		    \caption{Face Representation Using Different Haar Features}
		  \end{center}
		\end{figure}

		\par
		In order to be able to identify which areas of the image correspond to a Haar Feature, some steps need to be done. Firstly the image must be converted to a grayscale image (if it is in colour). This is because Haar Features are a composition of white and black areas, denoting lighter and darker areas in an object. In order to better identify these areas in the source, it is best to transform it into a format that accenuates lighter and brighter areas using different shades of black and white (just like our Haar Features). The next step is to normalize each pixel value in such a way to convert the 256 possible values of grayscale to a range between $[0,1]$, which better correspond to our Haar Feature representation. The final step would be that of comparing the region in question to the possible ideal feature we are trying to fit, and retrieve the Haar Value attributed to that region. In order to calculate the Haar Value;

			\begin{enumerate}
				\item Add the pixel values that correspond to the white region, of the Haar Feature we are trying to fit.
				\item Repeat the procedure for the area that corresponds to the black region in the Haar Feature.
				\item Subtract the white region value from the black region value and achieve the Haar Value.
			\end{enumerate}

		The Haar Value achieved should be in the region; $0<=Haar Value<=1$, where the closer the value is to 1, the more that Haar Feature is an ideal representation of that area. A treshold value is preset, to compare the Haar Value to in order to identify whether the Haar Feature represents the concerned area well enough. 

		\begin{figure}[ht]
		  \begin{center}
		    \includegraphics[width=0.6\linewidth]{calculating_haar.jpg}
		    \caption{Calculation of Haar Features}
		  \end{center}
		\end{figure}

	\subsection{Integral Images}

		\par
		As explained previously, in order to be able to fit Haar Features on different areas of different sizes in a source, the sliding window approach is endorsed. However using such an approach the calculations involved in resolving such a large amount of windows/concerned areas, would incurr a very high computation time. In fact it would take $O(n^2)$ to compute the Haar Values of a concerned window, since it is comprised of having to iterate through a 2d array. Since this has to be done for every window, the computation costs would be very expensive, and thus an alternative approach had to be found. The concept of integral images provided a solution to this. 

		\par
		In integral images we have a quick and effective way of calculating the sum of pixel values in an image or a subset of it. This is achieved by having each pixel represent the summation of all the pixel values above and to the left of it, and also include its value in the summation. 
	
		\begin{figure}[ht]
		  \begin{center}
		    \includegraphics[width=0.4\linewidth]{integral_images.jpg}
		    \caption{Calculation of an Integral Image}
		  \end{center}
		\end{figure}

		\par
		This concept proves to be very efficient for our cause, since in using the integral image approach we can calculate the value of any area desired, in constant time. Thus if applied to Haar Features, instead of having to iterate through a 2d array of pixels per sliding window in order to calculate the Haar Value, resulting in a computation of $O(n^2)$, we can just achieve the black region value and white region value (and consequently the Haar Value), in constant time. Therefore the first step conducted in the Viola\-Jones algorithm after, converting the image to normalized grayscale pixel values, is to calculate the integral image values based on those normalized values. Only after then, Haar Features fitting can commence. 

		\begin{figure}[ht]
			\begin{subfigure}{.5\textwidth}
			  \begin{center}
			    \includegraphics[width=0.6\linewidth]{integral_image_calculation2.jpg}
			    \caption{Calculating the Value of the Desired Area}
			  \end{center}
			\end{subfigure}
			\begin{subfigure}{.5\textwidth}
			  \begin{center}
			    \includegraphics[width=0.6\linewidth]{integral_image_calculation.jpg}
			    \caption{Calculating the Value of the Area Marked in Purple}
			  \end{center}
			\end{subfigure}
		\end{figure}

	\subsection{Training Function and Boosting}

	\par
	As mentioned in previous sections a source is comprised of a combination of different Haar Features or weak classifiers. If we take a 42 by 48 image the number of different possible combinations of features found (variation in size, position relative to the window, shape of the feature etc.) is more or less of 1,969,000. However it is obvious that not all of these features are suitable to represent the object that we are looking for in our sources. For such reason ADABoost algorithm is used in order to create a strong classifier form a number of weak classifiers (models that acheive accuracy just above random chance). 

	\par
	This is done by building a model using training data, made up of negative images which don't contain the objects that we are looking for as well as positive images of a size we want to train our classifier with (in our case we are using a 42 by 48 positive image training sample). In each iteration the best performing classifier is chosen to be part of the strong classifier, and the weight attributed to the training set is altered in such a way to enhance learning. The following is a pseudocode implementation of such algorithm.

	\begin{enumerate}
		\item Based on the size of the positive image training set, find all the possible features in such space
		\item Give all the positive and negative images an equal weight
		\item Normalize these weights in such a way to have a probability distribution
		\item For each feature possible (found in 1), train a classifier where you try to find the best treshold (haar value) that best classifies the images
		\item Calculate the error of classification each of these features has when solely trying to classify these images (weak classifier behaviour)
		\item Choose the feature with the minimum error rate, which will later be included in the strong classifier
		\item Update the weights of the images in such a way that the misclassfied images have a higher weight whilst correctly identified images have a lower weight
		\item Go back to step 4
	\end{enumerate} 

	\par
	The algorithm goes on untill the desired amount of weak classifiers/features is obtained to make up the strong classifier or the required error rate is met by the strong classifier. 

	\par
	Note that the step regarding the weight updates is done since misclassified images need to have higher influence on the training than the correctly classified ones to better correct the weak classifier error rates, and in doing so better identify the most significant features, that better classify the source.

	\subsection{Haar Cascades}

	\par
	In order to classify which areas in a source contain the object, the source is first split up in a series of sub windows, each being n by m in size, where n and m represent the size of the training images used in training to detect the possible Haar Features (in our case it would be 42 by 48).  Each of these sub windows is then scanned for the object we are trying to detect and if the object is found, it is classified as positive else it is classified as negative. In the next iteration the size of the sub windows is increased by a factor defined in the detection algorithm, and this is repeated up untill the maximum sub window size is reached. However as one can observe this will amount in a lot of computations. Furthermore with a normal approach, if the strong classifier consists of around 8000 features, all of the 8000 features have to be computed, per sub window, increasing even more the computation time, making the whole classification inefficient.

	\par
	To tackle such an issue, and whilst keeping in mind that most image regions consists of non face regions, the concept of cascade classifiers was introduced. Using this approach, instead of having to apply all of the 8000 features to each individual sub window, features are grouped into differene stages. The first stage would consist of a strong classifier made up of a small number of features, which however such an amount increases as the number of stages increases. Then each sub window is passed through each one of these stages, with each stage acting as a filter, and as each the stage number increases, the filtering criteria becomes more difficult and strict. As soon as a sub window is rejected at a specific stage, it is immediately omitted and classified as a negative sub window, which does not contain the object we are searching for. However if a sub window manages to go through all of the stages it is classified as a positive sub window. This approach is beneficial because it rejects many of the negative sub windows at a very early stage, whilst managing to keep the majority of the positive instances, and decreasing the computing power required as to when compared with the naive approach.

	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.8\linewidth]{cascade_classifier.jpg}
	    \caption{Cascade Classifier Work Flow}
	  \end{center}
	\end{figure}

\section{Artifact 1}
	
	\par
	For the first artifact, we had to train a Haar Cascade locally. OpenCv Project was used for this purpose. In order to be able to do this four steps had to be undertaken;

	\begin{enumerate}
		\item Collect a negative images dataset and create it's respective .info file
		\item Collect a positive images dataset and create it's respective .info file
		\item Create a vector file using the positive images data set
		\item Train Cascade using your data sets and prepared accompanying data
	\end{enumerate}

	\par
	A positive images dataset comprised of faces was obtained from \url{http://vision.ucsd.edu/~iskwak/ExtYaleDatabase/ExtYaleB.html}, whilst the dataset of non faces was obtained from \url{http://www.robots.ox.ac.uk/~vgg/data/background/background.tar}. It was important that the negative data set would be comprised of images that strictly do not contain the object we were trying to detect. Since we were training a Haar Cascade that recognized faces, we made sure that the negative data set didn't contain any faces. On the other hand the positive data set chosen was made up of cropped up frontal faces. Both of the data sets were in grayscale. The reason behind this decision was that since the training algorithm would need to work on images in grayscale, because of the way that Haar Features work, it would be optimal to find a data set that was already in the desired format. 

	\par
	The positive data set was chosen in such a way to have cropped faces and would only contain one subject per image to facilitate our training process. This is because in order to be able to begin training, the training algorithm necessitates a .info file that contains all the positive images path listed, together with the number of positive objects per image, their location with respect to x and y axis and the size of the object with respect to the previous coordinate. Since we only had one subject in the image and which was more or less the size of the whole image, we could write an easy script that would automate this .info file production. The data collection script operated by first going in the directory in which the positive images were located. Then it would iterate through each image's properties, and check whether it was valid or not. If the folder contained either a corrupted image, any irrelevant data file or data logs etc., these were removed from the database. Also images marked as "Ambient" were removed, since these were all blacked out images, which did not serve our purpose and instead would hinder the training process. Next the size of each photo in the cleaned up directory was noted, and the .info file was produced with the necessary data. From this file we could notice that our positive data set was made up of 2094 images.

		\begin{figure}[ht]
			\begin{subfigure}{.7\textwidth}
			  \begin{center}
			    \includegraphics[width=\linewidth]{db_cleaning_script.png}
			    \caption{Script \- Clean DB And Produce .info File}
			  \end{center}
			\end{subfigure}
			\hfill
			\begin{subfigure}{.3\textwidth}
			  \begin{center}
			    \includegraphics[width=\linewidth]{faces_info.png}
			    \caption{Sample Of Produced .info File}
			  \end{center}
			\end{subfigure}
		\end{figure}

	\par
	Another .info file was required for the negative images data set. However this time it did not need to have the size of the image specified etc. Instead only the location path of each image making up this database was required. Since all images were intact and no extra data files were present in the data set, a simple automation script was built. It's only purpose would be that of going through the folder containing all the negative images, and enlist the path of each in the appropriate .info file. From such .info file it could be observed that 900 samples made up our negative images data set. 

		\begin{figure}[ht]
			\begin{subfigure}{.7\textwidth}
			  \begin{center}
			    \includegraphics[width=0.8\linewidth]{bg_cleaning_script.png}
			    \caption{Script \- Clean DB And Produce .info File}
			  \end{center}
			\end{subfigure}
			\hfill
			\begin{subfigure}{.3\textwidth}
			  \begin{center}
			    \includegraphics[width=0.8\linewidth]{bg_info.png}
			    \caption{Sample Of Produced .info File}
			  \end{center}
			\end{subfigure}
		\end{figure}

	\par
	The size of both data sets was considered as satisfactory given that for our purposes the size of the positive data set was large enough. Furthermore if we consider the fact that it is recommended to have half the amount of negative images, with respect to positive images, the necessary criteria was met. Before we would be able to start the training process a final step was required; a vector file containing all the positive samples stitched together, with each sample being resized to a desired size. The opencv command \textbf{opencv\_createsamples -info faces\_info.info -num 2094 -w 42 -h 48 -vec faces.vec} was used. One should note that the info flag is used to denote the location of the previously built .info file containing the positive training images, the num flag denotes the amount of positive samples, w and h denote the size for the positive images to be resized to, whilst vec would denote the path for the vector file to be created in. The respective width and height were chosen for two reasons; the first reason being that faces are larger in height than in width by nature, and also if the size was chosen larger, the different possibilities of Haar Features would also increase, and thus making training more difficult in terms of computing power as well as it would take far longer to complete each stage. Also it was important that the size of the positive images was made to be smaller than that of the negative images because of the nature of the training algorithm, where the positive images would be super imposed on the negative ones.

	\par
	The final step would be that of starting training using the following opencv command; \textbf{opencv\_traincascade -data trained\_data -vec faces.vec -bg bg\_info.info -numPos 1800 -numNeg 800 -numStages 22 -w 42 -h 48}. The flags used have the following meaning:

	\begin{itemize}
		\item \-data: signifies the path that our trained classifier will be stored in
		\item \-vec: signifies the path in which our positive sample vector is found
		\item \-bg: signifies the path of the .info file that contains the information regarding the negative samples data set
		\item \-numPos: the amount of positive images in our data set
		\item \-numNeg: the amount of negative images in our data set
		\item \-numStages: the amount of stages we want our Haar Cascade to have 
		\item \-w: the width of the positive samples in our vector
		\item \-h: the height of the positive samples in our vector
	\end{itemize}

	\par
	One should note that the amount of positive and negative images signalled in the respective flag, should be less than the actual amount we have in our database due to some nitigrities that the OpenCv implementation has. Also, if we increase the number of stages for training, the classifier would become more accurate however it would take far more time to train. The next step to take is to let the training run for a couple of days untill finished. Once the training is completed the cascade will be found within the folder specified in the \-data flag.

	\par
	In order to test the cascade that was trained a script was produced, however it shall be discussed in the following section. The following is the result produced:

	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.8\linewidth]{output2.jpg}
	    \caption{Face Detection Using Locally Trained Classifier}
	  \end{center}
	\end{figure}

	\newpage 

\section{Artifact 2}

	\par
	For the second artifact, we had to obtain readily trained cascades from the opencv github; \url{https://github.com/opencv/opencv/tree/master/data/haarcascades}, rather than training our own, like we did in Artifact 1. In this section we will be using the readily trained cascades to identify faces and eyes in an inputted image. To be able to achieve this, a python script was developed, which will be explained in this section. Note that a similar script was used in the first Artifact to test the locally trained classifier. The main difference between the two scripts is the path of the classifier being used for detection. Note that even in this section, OpenCv project was used.

	\par
	The first section of the script involves that we check that OpenCv is properly configured on the local machine. Next we import the two trained cascades which we will be using for detection, mainly the frontal face cascade and the eye cascades. We will also be importing the image we will be using to test these cascades upon. Now we check that all the imports where done correctly, since without them the script would go into undefined behaviour. If not imported correctly, the user is alerted accordingly and the script shuts itself down.

	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.8\linewidth]{script1.png}
	    \caption{Setting Up}
	  \end{center}
	\end{figure}

	\par
	The second section consists of converting the imported image, into a grayscale image. This is done to agevolate Haar Feature identification. Next we split the inputted image into several smaller sub images, which will then go through the Strong Classifier which was imported at the beginning of the script. Those sub images, which are not omitted by the classifier are stored in a list structure which will be used later on. Note that parameters of the function that splits the input image into sub images plays a crucial role in detection. First of all the minimum size of the subwindow and the maximum size of the subwindow are defined according to the range of sizes a face present is expected to have. The scaling factor denotes the by how much the sliding window size should be increased. If the increase is too large, some misdetection could occur. Finally the amount of mininmum neighbours required denotes how many neighbours (aka sub images which are also believed to contain the feature we are looking for) a sliding window should have, if we want to consider it as a detected region. The smaller this number is the larger the amount of false positives there are. However if we increase the number by too much, overfitting would occurr, and features would go undetected. By trial and error, 5 was found to be the prefect amount of minNeighbours when using a scaling factor of 1.3.

	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.8\linewidth]{script2.png}
	    \caption{Face Detection Set Up}
	  \end{center}
	\end{figure}

	\par
	The next step would be that of highlighting the detected objects in the image, by iterating through the previously mentioned list and surrounding each sub window by a green box. However the task doesn't end here. Now we also have to check for any eyes present. Since there cannot be any eyes if there isn't a face, we start detecting eyes in the list of detected faces. In order to do so, a scale factor of 1.02 and 5 minNeighbours was deemed to be the best fit producing the best outcome. A list of found eyes was also returned, relative to the faces found, which was than iterated through and encapsulated in a red box. One should note that the reason behind such a small scaling factor is because of the sizes of eyes. Since eyes are small in nature, if a large scale factor would be used, these could be easily misclassified. Moreover the maximum window size allowed for eyes is set to be equal to the smallest size allowed for a face sub image. This is because it wouldn't make any sense to have an eye bigger than the smallest expected face. 

	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.8\linewidth]{script3.png}
	    \caption{Eye Detection Set Up And Marking Of Detected Objects}
	  \end{center}
	\end{figure}

	\par
	The final step would be that of saving the produced image consisting of the inputted image, together with the boxes surrounding the detected objects. Also such an image is displayed on screen for the user to inspect, and is kept open up untill the user presses any button. When such an event occurrs the scripts cleans up after itself and stops its execution.

	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.7\linewidth]{script4.png}
	    \caption{Producing Artifact}
	  \end{center}
	\end{figure}

	\par 
	The following is the output produced:

	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.7\linewidth]{output1.jpg}
	    \caption{Face And Eye Detection Using Online Classifiers}
	  \end{center}
	\end{figure}

\section{Experiments and Their Evaluations}

	\par
	From the Artifacts developed in the previous sections, one could notice that both artifacts had some misclassification in them. The following is an evaluation of each artifact produced.

	\subsection{Evaluation of Artifact 1}

		\par
		In the first artifact we had to train a Haar Cascade locally. This was quite time consuming and required a lot of computational power. However it was stopped at 22 stages since training was taking too long and was becoming infeasible to continue it. When testing the trained cascade produced, we could notice that out of the eight faces present in the picture, four were recognized, whilst the other four were not, giving four true positives and four false negatives. Notice that our cascade was only trained to detect faces and not eyes. Moreover there were also five false positives which were areas identified as faces, however they were not. Calculating the precision value we get a result of 44.444\%, whereas the recall value would be that of 50\%. Note that precision measures how accurately the model predicts it's values in terms of the percentage of correct positive predictions. This means that when our locally trained classifier classifies a sub image as the desired object, 44.44\% of the time that sub image actually contains the object. On the other hand recall is the measure of how good the model is at finding the positives. In our case, the model is able to find 50\% of the objects we are trying to detect. Various reasons could have contributed to these low results amongst which, we can find different shades, lighting present, focus on the faces,contrast with the background, size of the training samples etc.

	\subsection{Evaluation of Artifact 2}

		\par
		In the second artifact we produced an image with detected eyes and faces, using the online available trained cascades. These cascades were more successful and accurate than the ones locally trained, as it was expected. In fact when tested on the same image used to test our locally trained classifier, out of the eight faces present, all of the faces where detected. Furthermore there where no misclassifications with respect to faces. This would result in a 100\% precision and 100\% recall values, when it comes to face detection. However not the same can be said for the eyes. Out of sixteen eyes present in the picture, thirteen were correctly identified as true positives, whereas there were two misclassifications, where mouths were incorrectly identified as eyes being false positives, and three eyes not detected at all (false negatives). This would result in a precision of 86.67\%, where the classifier has 86.67\% chance of correctly detecting a sub image as an eye. The recall value is that of 81.25\%, which means that 81.25\% of all eyes in the image are detected when using this classifier. 

		\par
		The reason behind the false positives could have come because of the mouths being open in both cases. Thus there was a dark central area surrounded by a lighter reagion, which would provide the same properties that an eye would have. When it comes to the unidentified eyes, a pair of eyes belonged to the same person, who is wearing glasses in the picture, a factor that most probably has contributed towards the fact that the eye features were not detected and thus misclassified. With regards to the final eye not identified, reasons such as contrast and light could have affected, due to the nature of the photo used. 

	\subsection{Overall Evaluation}

		\par
		Overall it is evident that the classifiers found online outperformed the locally trained classifier by a lot. This could improve by having the locally trained classifier run for a longer time resulting in an increased amount of stages, and thus achieving a finetuned classifier. However this was not possible due to computing time and power, where the last stages were beginning to take an infeasible amount of time to train. This could be the reason why most companies do not share their trained classifiers, since these cost so much with respect to computation time and effort, as was experienced during this project. Other factors that could have improved the performance of these classifiers could be a larger data set containing a more varied set of negative images as well as a more varied set of positive images with respect to race of the people in the photos, angles of pose, face compostion and shape, lighting etc. One should note that the probabilities calculated over here are only with respect to a single run, using specific scaleFactors and minNeighbours. To achieve better and more representative conclusions, a mean average precision (maP) should be calculated.

\section{Alternative Algorithm}
	
	\subsection{Introducing KLT}

	\par
	Along the years, various learning algorithms have been developed to be able to detect and track facial features. This is mainly beacause of the importance that such technologies have in industry. In the previous sections, Viola Jones detection algorithm was evaluated. In this section we will be concentrating on another available technology for facial features tracking, mainly the Kanade Lucas Tomasi (KLT) algorithm.

	\par
	The KLT algorithm performs face detection and tracking mainly in two steps. Unlike Viola Jones, which is based on Haar Feature Detection and Strong Classifier training, KLT firstly performs feature extraction by performing a good features to track, feature detection algorithm. During this step, corner like structures called Harris Corners are detected in the first frame. The next step would be that of performing the LK method on these found feature points and start searching for positions that yield best match with regards to feature motion in the following frames. The LK method mainly consists of using spatial intensity as well as computation of motion and alignment vectors in order to track found features' optical flow, and producing new Harris Corners to track. This is done using Newton's method to minimize the sum of squared distances. Note that in order to be able to keep track of the newly found Harris Corners as well as the old ones, the step in which Harris Corner detection occurrs (first step) is re\-applied every few frames. 

		\begin{figure}[ht]
			\begin{subfigure}{.45\textwidth}
			  \begin{center}
			    \includegraphics[width=\linewidth]{mov_frames.jpg}
			    \caption{Movement Of Frames Wrt. Time}
			  \end{center}
			\end{subfigure}
			\hfill
			\begin{subfigure}{.55\textwidth}
			  \begin{center}
			    \includegraphics[width=\linewidth]{klt_feature_tracking.jpg}
			    \caption{Feature Tracking Wrt. Particular Instance}
			  \end{center}
			\end{subfigure}
		\end{figure}

	\par
	In having this approach, the Harris Corner detection step reduces by a large margin the features to track, since only representative features are extracted. Thus since the amount of features to track is reduced, and all non relevant features (such as the background's smooth edges) are ignored, the computational power needed to track these features, as well as the computational time it takes to track them, is greatly reduced. This makes the algorithm another fast option in the repertoire. 

	\subsection{Comparing KLT and Viola Jones}

	\par
	Comparing these two algorithms, based on various papers documenting experimentation and outcomes, it is clear that Viola Jones algorithm is not as effective in detecting tilted/turned/side/rotated faces. However it is very good at detecting frontal faces. Also it is somewhat sensitive to lighting conditions mainly because of the conversion to grayscale that is involved, where a variance in light may cause a Haar value not to pass the treshold set. Light variations occur because of various factors such as face orientation, contrast of face with regards to skin colour, and lighting provided itself. This may prove to be problematic also when trying to find features within other found features, such as trying to detect eyes within a facial region. Another problem with Viola Jones is that of different detections of the same object because of the sliding window approach. This may cause window overlap when detecting features concentrated in the same area, misinterpreting detection.

	\par
	On the other hand KLT better performs when it comes to face tilting/turning/orientation/rotation, because of the tracking algorithm which tracks the optical flow of concerned features. However the algorithm can lose track of certain features if there is a sudden large displacement between one frame and another. Moreover this algorithm behaves quite badly with dark images or images that lack in contrast. This is because of the fact that images having a low varying contrast appear as images having a smooth surface throught, making it very hard for the algorithm to identify Harris corners. Such an outcome can have disastrous affects especially if it's first stage is unable to locate a face in the first frame. This is because of the fact that if no Harris Corners are identified, the algorithm will have nothing to track, thus there will be no detection at all throughout. 

	\par
	An improvement on both algorithms could be that of having Viola Jones working on the first frame of the source to be detected. This would increase the probability of locating the object and thus ensuring that the KLT would not fail. If Viola Jones is followed by KLT, we would eliminate the lack of detection when it comes to different facial orientations, that Viola Jones suffers from, by being able to track relevant features found in the area of interest detected by Viola Jones as the first step of the newly proposed algorithm and thus achieving a possible better performing algorithm in terms of robustness.


\section{Overall Conclusions}
	
	In this project we introduced the importance of face detection in industry and the challenges these bring with them. Then we introduced the Viola Jones algorithm and how it operates, since it is one of the most simple but effective algorithms in this repertoire. To put the theory into practice we trained our own Haar Classifier using the Viola Jones algorithm and tested it on a specific image. Then the outcome was compared with the same image being tested using classifiers trained by the OpenCv project. The margin of difference between the outcomes was quite large, where the locally trained classifier had a precision of 44.444\% percent and a recall of 50\% for face detection, whilst the online frontal face classifier had a 100\% precision and recall. We also implemented a detector which would detect eyes, using an online eye classifier. The results for this where quite good as well where the precision was that of 86.67\% and a recall of 81.25\%. Finally the Viola Jones algorithm was compared against the KLT tracking and detection algorithm, where the pros and cons of each were identified, with the conclusion being that both perform really well in their respective stronger situations.
	
	\begin{figure}[ht]
	  \begin{center}
	    \includegraphics[width=0.8\linewidth]{completion.png}
	    \caption{Statement Of Completion Form}
	  \end{center}
	\end{figure}

	\includepdf{plaigarism.pdf}

\end{document}
