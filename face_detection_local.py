import cv2 as cv
import numpy

#importing locally trained classifier and create classifier object
haar_casc_faces_local = cv.CascadeClassifier("./trained_data/cascade.xml")


#check that classifiers are loaded objects are created correctly
if(haar_casc_faces_local.empty()):
	print("Could not import cascade. Check directory.")	
	sys.exit()

#getting an image as an input using opencv imread
current_frame = cv.imread("./images_to_test/cover.jpg")

#check that image is imported successfully
if current_frame is None:
	print("Could not import image. Check directory.")
	sys.exit()

#converting the current frame/img to grayscale, since cascades work on grayscale
gray_frame = cv.cvtColor(current_frame, cv.COLOR_BGR2GRAY)

#start detecting faces using locally trained classifier
faces_detected_local = haar_casc_faces_local.detectMultiScale(
	gray_frame,
	scaleFactor = 1.1,
	minNeighbors = 12, 
	minSize = (20,20),
	maxSize = (180,180))

#iterate through all the face rectangles recognized using locally trained classifier and draw a rectangle around them
for (x,y, width, height) in faces_detected_local:
	cv.rectangle(current_frame, (x,y), (x+width, y+height), (256,0,0), 4)

#writing image with detected faces
cv.imwrite("./produced_images/output2.jpg", current_frame)

#show modified frame with rectangles drawn on it, on screen
cv.imshow("Output", current_frame)

#wait for key to be pressed to close program
cv.waitKey(0)

print("Cleaned Up")
