import os
from PIL import Image

list_photos = os.listdir("../faces")
text_file = open("../faces_info.info", "w")
for photo_details in list_photos:
	if "bad" in photo_details or "Ambient" in photo_details or "info" in photo_details or "log" in photo_details or "LOG" in photo_details:
		os.remove("/home/franklyn07/face_detect/faces/"+photo_details)
	else:
		with Image.open("./faces/" + photo_details) as img:
			width, height = img.size
			text_file.write("./faces/" + photo_details + " 1 0 0 %d %d\n"% (width, height))
text_file.close()
