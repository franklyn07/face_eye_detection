import cv2 as cv
import numpy
import sys

#check that cv is imported correctly
print (cv.__version__)

#importing pre defined face and eye cascades and create a cascade classifier object
haar_casc_faces = cv.CascadeClassifier("./haar_cascades/haarcascade_frontalface_default.xml")
haar_casc_eyes = cv.CascadeClassifier("./haar_cascades/haarcascade_eye.xml")

#check that classifiers are loaded objects are created correctly
if(haar_casc_eyes.empty() or haar_casc_faces.empty()):
	print("Could not import cascades. Check directory.")	
	sys.exit()

#getting an image as an input using opencv imread
current_frame = cv.imread("./images_to_test/cover.jpg")

#check that image is imported successfully
if current_frame is None:
	print("Could not import image. Check directory.")
	sys.exit()

#converting the current frame/img to grayscale, since cascades work on grayscale
gray_frame = cv.cvtColor(current_frame, cv.COLOR_BGR2GRAY)


#start detecting objects in the current frame read - note that a list of rectangles of the detected objectes
#is returned. In our case a list of rectangles containing faces will be returned since we are using face cascades.
#note that face cascades used here are the ones imported not the ones trained locally
faces_detected = haar_casc_faces.detectMultiScale(
	gray_frame,
	#resizing scale factor
	scaleFactor = 1.3,
	minNeighbors = 5,
	#min and max box size that considered as faces 
	minSize = (60,60),
	maxSize = (335,335))

#iterate through all the face rectangles recognized and draw a rectangle around them
for (x,y, width, height) in faces_detected:
	cv.rectangle(current_frame, (x,y), (x+width, y+height), (0,256,0), 4)
	#creating an isolated frame to perform further detection
	#eyes are only in detected faces - we can only search for them in detected faces	
	face_frame_gray = gray_frame[y:y+width, x:x+width]
	face_frame_color = current_frame[y:y+width, x:x+width]
	#detecting eyes in the isolated frame
	eyes_detected = haar_casc_eyes.detectMultiScale(
		face_frame_gray,
		#notice that the scale factor is smaller here.
		#since we have a small frame we can afford to do that
		#even more eyes are small, thus we should increment slowly
		scaleFactor = 1.02,
		minNeighbors = 5,
		#small size since eyes can be very small
		#also note that the max eyes captured shouldn't be larger than the smallest possible face 
		minSize = (5,5),
		maxSize = (60,60))
	#iterate to draw rectangles around found eyes
	for(eyes_x,eyes_y,eyes_width,eyes_height) in eyes_detected:
		cv.rectangle(face_frame_color,(eyes_x,eyes_y), (eyes_x+eyes_width,eyes_y+eyes_height), (0,0,256),2)

#writing image with detected faces
cv.imwrite("./produced_images/output1.jpg", current_frame)

#show modified frame with rectangles drawn on it, on screen
cv.imshow("Output", current_frame)

#wait for key to be pressed to close program
cv.waitKey(0)

print("Cleaned Up")
