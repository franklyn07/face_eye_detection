import cv2 as cv
import numpy
import sys

#check that cv is imported correctly
print (cv.__version__)

#importing pre defined face and eye cascades and create a cascade classifier object
haar_casc_faces = cv.CascadeClassifier("./trained_data/cascade.xml")


#check that classifiers are loaded objects are created correctly
if(haar_casc_faces.empty()):
	print("Could not import cascades. Check directory.")	
	sys.exit()

#getting an image as an input using opencv imread
current_frame = cv.imread("./images_to_test/cover.jpg")

#converting the current frame/img to grayscale, since cascades work on grayscale
gray_frame = cv.cvtColor(current_frame, cv.COLOR_BGR2GRAY)

#start detecting objects in the current frame read - note that a list of rectangles of the detected objectes
#is returned. In our case a list of rectangles containing faces will be returned since we are using face cascades
faces_detected = haar_casc_faces.detectMultiScale(
	gray_frame,
	scaleFactor = 1.1,
	minNeighbors = 8, 
	minSize = (20,20),
	maxSize = (180,180))

#iterate through all the face rectangles recognized and draw a rectangle around them
for (x,y, width, height) in faces_detected:
	cv.rectangle(current_frame, (x,y), (x+width, y+height), (0,128,0), 4)

#show modified frame with rectangles drawn on it, on screen
cv.imshow("Output", current_frame)

#wait for key to be pressed to close program
cv.waitKey(0)

print("Cleaned Up")
